Sujet 1 : Python
Écrire une fonction qui prend un paramètre chaîne de caractère, et qui
renvoi cette chaine en majuscule, sans utiliser la fonction majus
directement.

Sujet 2 : Python
Écrire une fonction qui prend un paramètre entier X, et qui renvoi une
réponse booléenne pour dire si le chiffre est divisible par 3 ou pas,
sans utiliser la fonction div ou mod ou les opérateurs de division et
reste de la division.

Sujet 3 : Python
Plateau= [ [True , False , False , False ] ,
                 [ False , True , True , False ] ]
Un plateau est un tableau à deux dimensions qui contient des Booléens.
Si plateau[i][j]
vaut True, il y a un mur et sinon la case est libre.
Le but de l'exercice est d'écrire une fonction chemin prenant un
plateau et les coordonnées de deux cases deb et fin et renvoie True si
l'on peut aller de la case deb à la case fin en se déplaçant
horizontalement et verticalement.
Dans notre exemple, chemin(plateau,(1,3),(1,0)) renverra False et
chemin(plateau,(1,3),(0,1)) renverra True.

Ecrire une fonction voisinsCase qui prend un plateau et une case et
renvoie l'ensemble de ces voisins immédiats horizontaux ou verticaux
qui sont sur le plateau et qui sont libres.
Ecrire une fonction voisinsCases qui prend un plateau et un ensemble
de cases et renvoie l'ensemble de tous les voisins de ces cases.
Ecrire une fonction accessibles qui prend un plateau et une case et
renvoie l'ensemble des cases que l'on peut atteindre depuis cette case
en se déplaçant horizontalement et verticalement.
Ecrire la fonction chemin.


Sujet 4 : Linux
Comment on planifie une tâche hebdomadaire sur un serveur

Sujet 5 : GIT

Sur quelle base on nomme une branche sur GIT (il n'y a une mauvaise
réponse, c'est pour voir comment vous avez travaillé)
Pourquoi on travaille avec la notion des branches ?
Comment vous commentez un commit